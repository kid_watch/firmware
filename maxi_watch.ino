#include <SPI.h>
#include <stdint.h>
#include <BLEPeripheral.h>

#include <nrf_nvic.h>//interrupt controller stuff
#include <nrf_sdm.h>
#include <nrf_soc.h>
#include <WInterrupts.h>
#include "Adafruit_GFX.h"
#include "FreeSans9pt7b.h"
#include "SSD1306.h"
#include <TimeLib.h>
#include <nrf.h>
#include "count_steps.h"
#include "count_steps.c"
#include "i2csoft.h"

#include "icons/icons.h"

#define wdt_reset() NRF_WDT->RR[0] = WDT_RR_RR_Reload
#define wdt_enable(timeout) \
  NRF_WDT->CONFIG = NRF_WDT->CONFIG = (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos) | ( WDT_CONFIG_SLEEP_Pause << WDT_CONFIG_SLEEP_Pos); \
  NRF_WDT->CRV = (32768*timeout)/1000; \
  NRF_WDT->RREN |= WDT_RREN_RR0_Msk;  \
  NRF_WDT->TASKS_START = 1

Adafruit_SSD1306 display(128, 32, &SPI, 28, 4, 29);

boolean debug = false;

#define sleepDelay 7000
#define BUTTON_PIN              30
#define refreshRate 100

int menu;
volatile bool buttonPressed = false;
long startbutton;
unsigned long sleepTime, displayRefreshTime;
volatile bool sleeping = false;
int timezone;
int steps;
int steps1;
String serialNr = "235246472";
String versionNr = "110.200.051";
String btversionNr = "100.016.051";
String msgText;
boolean gotoBootloader = false;
boolean vibrationMode;

String bleSymbol = " ";
int contrast;

BLEPeripheral       blePeripheral           = BLEPeripheral();
BLEService          batteryLevelService     = BLEService("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
BLECharacteristic   TXchar        = BLECharacteristic("6E400003-B5A3-F393-E0A9-E50E24DCCA9E", BLENotify, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);
BLECharacteristic   RXchar        = BLECharacteristic("6E400002-B5A3-F393-E0A9-E50E24DCCA9E", BLEWriteWithoutResponse, BLE_ATTRIBUTE_MAX_VALUE_LENGTH);

enum ICONS {
  NONE,
  PIZZA,
  BED,
  BROOM,
  SHIRT,
  PUZZLE,
  WALK,
  TREE,
  HAMBURGER,
  GUITAR,
  DRUM,
  CHESS,
  CAT,
  MUSIC,
  CANDY_CANE,
  BOOK,
  BREAD,
  COOKIE,
  DRUMSTICK,
  GHOST,
  BABY,
  STROLLER,
  BACON,
  BIKE,
  SCOOTER,
  SHOWER,
  CARROT,
  TOOTHBRUSH,
  RUNNING,
  WASH_HANDS,
  TV
};

enum ICONS current_icon;
String current_msg = "";
int points = 0;

#define N_GRAINS     253 // Number of grains of sand
#define WIDTH        127 // Display width in pixels
#define HEIGHT       32 // Display height in pixels
#define MAX_FPS      150 // Maximum redraw rate, frames/second

// The 'sand' grains exist in an integer coordinate space that's 256X
// the scale of the pixel grid, allowing them to move and interact at
// less than whole-pixel increments.
#define MAX_X (WIDTH  * 256 - 1) // Maximum X coordinate in grain space
#define MAX_Y (HEIGHT * 256 - 1) // Maximum Y coordinate
struct Grain {
  int16_t  x,  y; // Position
  int16_t vx, vy; // Velocity
  uint16_t pos;
} grain[N_GRAINS];

uint32_t        prevTime   = 0;      // Used for frames-per-second throttle
uint16_t         backbuffer = 0, img[WIDTH * HEIGHT]; // Internal 'map' of pixels

#ifdef __cplusplus
extern "C" {
#endif

#define LF_FREQUENCY 32768UL
#define SECONDS(x) ((uint32_t)((LF_FREQUENCY * x) + 0.5))
#define wakeUpSeconds 120
void RTC2_IRQHandler(void)
{
  volatile uint32_t dummy;
  if (NRF_RTC2->EVENTS_COMPARE[0] == 1)
  {
    NRF_RTC2->EVENTS_COMPARE[0] = 0;
    NRF_RTC2->CC[0] = NRF_RTC2->COUNTER +  SECONDS(wakeUpSeconds);
    dummy = NRF_RTC2->EVENTS_COMPARE[0];
    dummy;
    //powerUp();
  }
}

void initRTC2() {

  NVIC_SetPriority(RTC2_IRQn, 15);
  NVIC_ClearPendingIRQ(RTC2_IRQn);
  NVIC_EnableIRQ(RTC2_IRQn);

  NRF_RTC2->PRESCALER = 0;
  NRF_RTC2->CC[0] = SECONDS(wakeUpSeconds);
  NRF_RTC2->INTENSET = RTC_EVTENSET_COMPARE0_Enabled << RTC_EVTENSET_COMPARE0_Pos;
  NRF_RTC2->EVTENSET = RTC_INTENSET_COMPARE0_Enabled << RTC_INTENSET_COMPARE0_Pos;
  NRF_RTC2->TASKS_START = 1;
}
#ifdef __cplusplus
}
#endif

void powerUp() {
  if (sleeping) {
    sleeping = false;
    display.begin(SSD1306_SWITCHCAPVCC);
    display.clearDisplay();
    display.display();
    if (debug)Serial.begin(115200);

    delay(5);
  }
  sleepTime = millis();
}

void powerDown() {
  if (!sleeping) {
    if (debug)NRF_UART0->ENABLE = UART_ENABLE_ENABLE_Disabled;
    sleeping = true;

    digitalWrite(28, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(29, LOW);
    digitalWrite(4, LOW);
    NRF_SAADC ->ENABLE = 0; //disable ADC
    NRF_PWM0  ->ENABLE = 0; //disable all pwm instance
    NRF_PWM1  ->ENABLE = 0;
    NRF_PWM2  ->ENABLE = 0;
  }
}

void charge() {
  if (sleeping)menu = 88;
  powerUp();
}

void buttonHandler() {
  if (!sleeping) buttonPressed = true;
  else menu = 0;
  powerUp();
}

void acclHandler() {
  ReadRegister(0x17);
  if (sleeping) {
    powerUp();
  }
}

void blePeripheralConnectHandler(BLECentral& central) {
  if (debug)Serial.println("BLEconnected");
  menu = 0;
  powerUp();
  bleSymbol = "B";
}

void blePeripheralDisconnectHandler(BLECentral& central) {
  if (debug)Serial.println("BLEdisconnected");
  menu = 0;
  powerUp();
  bleSymbol = " ";
}

void setIcon(String icon_name) {
  if(icon_name == "PIZZA") {
    current_icon = PIZZA;
  } else if (icon_name == "SCOOTER") {
    current_icon = SCOOTER;
  } else if (icon_name == "BED") {
    current_icon = BED;
  } else if (icon_name == "SHOWER") {
    current_icon = SHOWER;
  } else if (icon_name == "NONE") {
    current_icon = NONE;
  } else if (icon_name == "BROOM") {
    current_icon = BROOM;
  } else if (icon_name == "BOOK") {
    current_icon = BOOK;
  } else if (icon_name == "TBRUSH") {
    current_icon = TOOTHBRUSH;
  } else if (icon_name == "SHIRT") {
    current_icon = SHIRT;
  } else if (icon_name == "PUZZLE") {
    current_icon = PUZZLE;
  } else if (icon_name == "WALK") {
    current_icon = WALK;
  } else if (icon_name == "GUITAR") {
    current_icon = GUITAR;
  }  else if (icon_name == "CARROT") {
    current_icon = CARROT;
  } else if (icon_name == "BACON") {
    current_icon = BACON;
  } else if (icon_name == "BREAD") {
    current_icon = BREAD;
  } else if (icon_name == "MUSIC") {
    current_icon = MUSIC;
  } else if (icon_name == "STROLLER") {
    current_icon = STROLLER;
  } else if (icon_name == "BOOK") {
    current_icon = BOOK;
  } else if (icon_name == "TREE") {
    current_icon = TREE;
  } else if (icon_name == "COOKIE") {
    current_icon = COOKIE;
  } else if (icon_name == "RUNNING") {
    current_icon = RUNNING;
  } else if (icon_name == "HWASH") {
    current_icon = WASH_HANDS;
  } else if (icon_name == "TV") {
    current_icon = TV;
  }
}

String answer = "";
String tempCmd = "";
int tempLen = 0, tempLen1;
boolean syn;

void characteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  char remoteCharArray[21];
  tempLen1 = characteristic.valueLength();
  tempLen = tempLen + tempLen1;
  memset(remoteCharArray, 0, sizeof(remoteCharArray));
  memcpy(remoteCharArray, characteristic.value(), tempLen1);
  tempCmd = tempCmd + remoteCharArray;
  if (tempCmd[tempLen - 1] == '\n') {
    answer = tempCmd.substring(0, tempLen - 1);
    tempCmd = "";
    tempLen = 0;
    if (debug)Serial.print("RxBle: ");
    if (debug)Serial.println(answer);
    filterCmd(answer);
  }
}

void setPoints(String new_val) {
  points = new_val.toInt();
}

void pointUp() {
  points++;
}
void pointDown() {
  points--;
  if(points < 0) {
    points = 0;
  }
}

void filterCmd(String Command) {
  if (Command == "AT+BOND") {
    sendBLEcmd("AT+BOND:OK");
  } else if (Command == "AT+ACT") {
    sendBLEcmd("AT+ACT:0");
  } else if (Command == "BT+UPGB") {
    gotoBootloader = true;
    NRF_POWER->GPREGRET = 0x01;
    sd_nvic_SystemReset();
  } else if (Command == "BT+RESET") {
    sd_nvic_SystemReset();
  } else if (Command.substring(0, 8) == "AT+ICON=") {
    sendBLEcmd("AT+ICON:" + Command.substring(8));
    setIcon(Command.substring(8));
  } else if (Command.substring(0, 10) == "AT+POINTS=") {
    sendBLEcmd("AT+POINTS:" + Command.substring(10));
    setPoints(Command.substring(10));
  } else if (Command == "AT+GPOINTS") {
    sendBLEcmd("AT+GPOINTS:" + points);
  } else if (Command == "AT+PUP") {
    pointUp();
    sendBLEcmd("AT+PUP:" + points);
  } else if (Command == "AT+PDOWN") {
    pointDown();
    sendBLEcmd("AT+PDOWN:" + points);
  } else if (Command == "AT+BATT") {
    sendBLEcmd("AT+BATT:" + String(getBatteryLevel()));
  } else if (Command == "BT+VER") {
    sendBLEcmd("BT+VER:" + btversionNr);
  } else if (Command == "AT+VER") {
    sendBLEcmd("AT+VER:" + versionNr);
  } else if (Command == "AT+SN") {
    sendBLEcmd("AT+SN:" + serialNr);
  } else if (Command.substring(0, 10) == "AT+MOTOR=1") {
    sendBLEcmd("AT+MOTOR:1" + Command.substring(10));
    digitalWrite(25, HIGH);
    delay(300);
    digitalWrite(25, LOW);
  } else if (Command.substring(0, 6) == "AT+DT=") {
    SetDateTimeString(Command.substring(6));
    sendBLEcmd("AT+DT:" + GetDateTimeString());
  }
}

void sendBLEcmd(String Command) {
  if (debug)Serial.print("TxBle: ");
  if (debug)Serial.println(Command);
  Command = Command + "\r\n";
  while (Command.length() > 0) {
    const char* TempSendCmd;
    String TempCommand = Command.substring(0, 20);
    TempSendCmd = &TempCommand[0];
    TXchar.setValue(TempSendCmd);
    //TXchar1.setValue(TempSendCmd);
    Command = Command.substring(20);
  }
}

String GetDateTimeString() {
  String datetime = String(year());
  if (month() < 10) datetime += "0";
  datetime += String(month());
  if (day() < 10) datetime += "0";
  datetime += String(day());
  if (hour() < 10) datetime += "0";
  datetime += String(hour());
  if (minute() < 10) datetime += "0";
  datetime += String(minute());
  return datetime;
}

void SetDateTimeString(String datetime) {
  int year = datetime.substring(0, 4).toInt();
  int month = datetime.substring(4, 6).toInt();
  int day = datetime.substring(6, 8).toInt();
  int hr = datetime.substring(8, 10).toInt();
  int min = datetime.substring(10, 12).toInt();
  int sec = datetime.substring(12, 14).toInt();
  setTime( hr, min, sec, day, month, year);
}

void handlePush(String pushMSG) {
  int commaIndex = pushMSG.indexOf(',');
  int secondCommaIndex = pushMSG.indexOf(',', commaIndex + 1);
  int lastCommaIndex = pushMSG.indexOf(',', secondCommaIndex + 1);
  String MsgText = pushMSG.substring(commaIndex + 1, secondCommaIndex);
  int timeShown = pushMSG.substring(secondCommaIndex + 1, lastCommaIndex).toInt();
  int SymbolNr = pushMSG.substring(lastCommaIndex + 1).toInt();
  msgText = MsgText;
  if (debug)Serial.println("MSGtext: " + msgText);
  if (debug)Serial.println("symbol: " + String(SymbolNr));
}

int getBatteryLevel() {
  return map(analogRead(3), 500, 715, 0, 100);
}

void setup() {
  current_icon = SCOOTER;
  setTime(17, 03, 00, 04, 04, 2020);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(3, INPUT);
  if (digitalRead(BUTTON_PIN) == LOW) {
    NRF_POWER->GPREGRET = 0x01;
    sd_nvic_SystemReset();
  }
  pinMode(2, INPUT);
  pinMode(25, OUTPUT);
  digitalWrite(25, HIGH);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(15, INPUT);
  if (debug)Serial.begin(115200);
  wdt_enable(7000);
  blePeripheral.setLocalName("Max-watch");
  blePeripheral.setAdvertisingInterval(555);
  blePeripheral.setAppearance(0x0000);
  blePeripheral.setConnectable(true);
  blePeripheral.setDeviceName("ATCDSD6");
  blePeripheral.setAdvertisedServiceUuid(batteryLevelService.uuid());
  blePeripheral.addAttribute(batteryLevelService);
  blePeripheral.addAttribute(TXchar);
  blePeripheral.addAttribute(RXchar);
  RXchar.setEventHandler(BLEWritten, characteristicWritten);
  /*
  blePeripheral.setAdvertisedServiceUuid(batteryLevelService1.uuid());
  blePeripheral.addAttribute(batteryLevelService1);
  blePeripheral.addAttribute(TXchar1);
  blePeripheral.addAttribute(RXchar1);
  RXchar1.setEventHandler(BLEWritten, characteristicWritten);
  */
  blePeripheral.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  blePeripheral.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);
  blePeripheral.begin();
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonHandler, FALLING);
  attachInterrupt(digitalPinToInterrupt(15), acclHandler, RISING);
  NRF_GPIO->PIN_CNF[15] &= ~((uint32_t)GPIO_PIN_CNF_SENSE_Msk);
  NRF_GPIO->PIN_CNF[15] |= ((uint32_t)GPIO_PIN_CNF_SENSE_High << GPIO_PIN_CNF_SENSE_Pos);
  attachInterrupt(digitalPinToInterrupt(2), charge, RISING);
  NRF_GPIO->PIN_CNF[2] &= ~((uint32_t)GPIO_PIN_CNF_SENSE_Msk);
  NRF_GPIO->PIN_CNF[2] |= ((uint32_t)GPIO_PIN_CNF_SENSE_High << GPIO_PIN_CNF_SENSE_Pos);
  display.begin(SSD1306_SWITCHCAPVCC);
  delay(100);
  display.clearDisplay();
  // display.setFont(&FreeSerifItalic9pt7b);
  display.display();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(10, 0);
  display.println("D6 Emulator");
  display.display();
  digitalWrite(25, LOW);
  sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
  initRTC2();
  initi2c();
  initkx023();

  uint8_t i, j, bytes;
  memset(img, 0, sizeof(img)); // Clear the img[] array
  for (i = 0; i < N_GRAINS; i++) { // For each sand grain...
    do {
      grain[i].x = random(WIDTH  * 256); // Assign random position within
      grain[i].y = random(HEIGHT * 256); // the 'grain' coordinate space
      // Check if corresponding pixel position is already occupied...
      for (j = 0; (j < i) && (((grain[i].x / 256) != (grain[j].x / 256)) ||
                              ((grain[i].y / 256) != (grain[j].y / 256))); j++);
    } while (j < i); // Keep retrying until a clear spot is found
    img[(grain[i].y / 256) * WIDTH + (grain[i].x / 256)] = 255; // Mark it
    grain[i].pos = (grain[i].y / 256) * WIDTH + (grain[i].x / 256);
    grain[i].vx = grain[i].vy = 0; // Initial velocity is zero
  }
}

void displayIcon(int x, int y, ICONS icon) {
  switch(icon) {
    case SCOOTER:
      display.drawXBitmap(x, y, (uint8_t*)scooter_bits, 32, 32, 1);
      break;
    case SHOWER:
      display.drawXBitmap(x, y, (uint8_t*)shower_bits, 32, 32, 1);
      break;
    case CARROT:
      display.drawXBitmap(x, y, (uint8_t*)carrot_bits, 32, 32, 1);
      break;
    case BED:
      display.drawXBitmap(x, y, (uint8_t*)bed_bits, 32, 32, 1);
      break;
    case PIZZA:
      display.drawXBitmap(x, y, (uint8_t*)pizza_slice_bits, 32, 32, 1);
      break;
    case NONE:
      //display.drawXBitmap(x, y, (uint8_t*)pizza_slice_bits, 32, 32, 1);
      break;
    case TOOTHBRUSH:
      display.drawXBitmap(x, y, (uint8_t*)toothbrush_bits, 32, 32, 1);
      break;
    case WALK:
      display.drawXBitmap(x, y, (uint8_t*)walking_bits, 32, 32, 1);
      break;
    case PUZZLE:
      display.drawXBitmap(x, y, (uint8_t*)puzzle_piece_bits, 32, 32, 1);
      break;
    case SHIRT:
      display.drawXBitmap(x, y, (uint8_t*)tshirt_bits, 32, 32, 1);
      break;
    case BACON:
      display.drawXBitmap(x, y, (uint8_t*)bacon_bits, 32, 32, 1);
      break;
    case BREAD:
      display.drawXBitmap(x, y, (uint8_t*)bread_slice_bits, 32, 32, 1);
      break;
    case STROLLER:
      display.drawXBitmap(x, y, (uint8_t*)baby_carriage_bits, 32, 32, 1);
      break;
    case GUITAR:
      display.drawXBitmap(x, y, (uint8_t*)guitar_bits, 32, 32, 1);
      break;
    case MUSIC:
      display.drawXBitmap(x, y, (uint8_t*)music_bits, 32, 32, 1);
      break;
    case BOOK:
      display.drawXBitmap(x, y, (uint8_t*)book_bits, 32, 32, 1);
      break;
    case TREE:
      display.drawXBitmap(x, y, (uint8_t*)tree_bits, 32, 32, 1);
      break;
    case COOKIE:
      display.drawXBitmap(x, y, (uint8_t*)cookie_bite_bits, 32, 32, 1);
      break;
     case RUNNING:
      display.drawXBitmap(x, y, (uint8_t*)running_bits, 32, 32, 1);
      break;
    case WASH_HANDS:
      display.drawXBitmap(x, y, (uint8_t*)hands_wash_bits, 32, 32, 1);
      break;
    case TV:
      display.drawXBitmap(x, y, (uint8_t*)tv_bits, 32, 32, 1);
      break;
  }
}

void displayPoints() {
  //display.setFont(FreeSans9pt7b);
  display.setRotation(3);
  display.clearDisplay();
  display.setCursor(0, 0);
  displayIcon(0,0, COOKIE);

  display.setCursor(0, 50);
  display.setTextSize(2);
  display.println(points);

  display.setTextSize(1);
  display.setCursor(0, 75);
  display.println("POINTS");

  display.display();
}

void displayTest() {
  display.setRotation(3);
  display.clearDisplay();
  display.setCursor(0, 0);
  //displayIcon(0,0, current_icon);

  display.setCursor(0, 40);
  display.setTextSize(2);
  display.println("OK");
  
 
  display.display();
}

void displayMain() {
  //display.setFont(FreeSans9pt7b);
  display.setRotation(3);
  display.clearDisplay();
  display.setCursor(0, 0);
  displayIcon(0,0, current_icon);

  display.setCursor(0, 40);
  display.setTextSize(2);
  int hr = hour();
  if(hr > 12) { 
    hr = hr - 12;
  }
  if(hr == 0) {
    hr = 12;
  }
  display.println(hr);

  //display.setCursor(0, 43);
  if (minute() < 10) {
    display.print("0");
  }
  display.println(minute());

  /*
  display.setTextSize(1);
  display.setCursor(0, 90);
  display.print(getBatteryLevel());
  display.print("%");
  */
 
  display.display();
}

void displaySand() {
  uint32_t t;
  while (((t = micros()) - prevTime) < (1000000L / MAX_FPS));
  prevTime = t;

  uint8_t res[6];
  softbeginTransmission(0x1F);
  softwrite(0x06);
  softendTransmission();
  softrequestFrom(0x1F , 6);
  res[0] = softread();
  res[1] = softread();
  res[2] = softread();
  res[3] = softread();
  res[4] = softread();
  res[5] = softread();
  int x = (int16_t)((res[1] << 8) | res[0]);
  int y = (int16_t)((res[3] << 8) | res[2]);
  int z = (int16_t)((res[5] << 8) | res[4]);

  float accelX = y;
  float accelY = -x;
  float accelZ = z;
  int16_t ax = -accelY / 256,      // Transform accelerometer axes
          ay =  accelX / 256,      // to grain coordinate space
          az = abs(accelZ) / 2048; // Random motion factor
  az = (az >= 3) ? 1 : 4 - az;      // Clip & invert
  ax -= az;                         // Subtract motion factor from X, Y
  ay -= az;
  int16_t az2 = az * 2 + 1;         // Range of random motion to add back in

  int32_t v2; // Velocity squared
  float   v;  // Absolute velocity
  for (int i = 0; i < N_GRAINS; i++) {
    grain[i].vx += ax + random(az2); // A little randomness makes
    grain[i].vy += ay + random(az2); // tall stacks topple better!
    v2 = (int32_t)grain[i].vx * grain[i].vx + (int32_t)grain[i].vy * grain[i].vy;
    if (v2 > 65536) { // If v^2 > 65536, then v > 256
      v = sqrt((float)v2); // Velocity vector magnitude
      grain[i].vx = (int)(256.0 * (float)grain[i].vx / v); // Maintain heading
      grain[i].vy = (int)(256.0 * (float)grain[i].vy / v); // Limit magnitude
    }
  }

  uint16_t        i, bytes, oldidx, newidx, delta;
  int16_t        newx, newy;

  for (i = 0; i < N_GRAINS; i++) {
    newx = grain[i].x + grain[i].vx; // New position in grain space
    newy = grain[i].y + grain[i].vy;
    if (newx > MAX_X) {              // If grain would go out of bounds
      newx         = MAX_X;          // keep it inside, and
      grain[i].vx /= -2;             // give a slight bounce off the wall
    } else if (newx < 0) {
      newx         = 0;
      grain[i].vx /= -2;
    }
    if (newy > MAX_Y) {
      newy         = MAX_Y;
      grain[i].vy /= -2;
    } else if (newy < 0) {
      newy         = 0;
      grain[i].vy /= -2;
    }

    oldidx = (grain[i].y / 256) * WIDTH + (grain[i].x / 256); // Prior pixel #
    newidx = (newy      / 256) * WIDTH + (newx      / 256); // New pixel #
    if ((oldidx != newidx) && // If grain is moving to a new pixel...
        img[newidx]) {       // but if that pixel is already occupied...
      delta = abs(newidx - oldidx); // What direction when blocked?
      if (delta == 1) {           // 1 pixel left or right)
        newx         = grain[i].x;  // Cancel X motion
        grain[i].vx /= -2;          // and bounce X velocity (Y is OK)
        newidx       = oldidx;      // No pixel change
      } else if (delta == WIDTH) { // 1 pixel up or down
        newy         = grain[i].y;  // Cancel Y motion
        grain[i].vy /= -2;          // and bounce Y velocity (X is OK)
        newidx       = oldidx;      // No pixel change
      } else { // Diagonal intersection is more tricky...
        if ((abs(grain[i].vx) - abs(grain[i].vy)) >= 0) { // X axis is faster
          newidx = (grain[i].y / 256) * WIDTH + (newx / 256);
          if (!img[newidx]) { // That pixel's free!  Take it!  But...
            newy         = grain[i].y; // Cancel Y motion
            grain[i].vy /= -2;         // and bounce Y velocity
          } else { // X pixel is taken, so try Y...
            newidx = (newy / 256) * WIDTH + (grain[i].x / 256);
            if (!img[newidx]) { // Pixel is free, take it, but first...
              newx         = grain[i].x; // Cancel X motion
              grain[i].vx /= -2;         // and bounce X velocity
            } else { // Both spots are occupied
              newx         = grain[i].x; // Cancel X & Y motion
              newy         = grain[i].y;
              grain[i].vx /= -2;         // Bounce X & Y velocity
              grain[i].vy /= -2;
              newidx       = oldidx;     // Not moving
            }
          }
        } else { // Y axis is faster, start there
          newidx = (newy / 256) * WIDTH + (grain[i].x / 256);
          if (!img[newidx]) { // Pixel's free!  Take it!  But...
            newx         = grain[i].x; // Cancel X motion
            grain[i].vy /= -2;         // and bounce X velocity
          } else { // Y pixel is taken, so try X...
            newidx = (grain[i].y / 256) * WIDTH + (newx / 256);
            if (!img[newidx]) { // Pixel is free, take it, but first...
              newy         = grain[i].y; // Cancel Y motion
              grain[i].vy /= -2;         // and bounce Y velocity
            } else { // Both spots are occupied
              newx         = grain[i].x; // Cancel X & Y motion
              newy         = grain[i].y;
              grain[i].vx /= -2;         // Bounce X & Y velocity
              grain[i].vy /= -2;
              newidx       = oldidx;     // Not moving
            }
          }
        }
      }
    }
    grain[i].x  = newx; // Update grain position
    grain[i].y  = newy;
    img[oldidx] = 0;    // Clear old spot (might be same as new, that's OK)
    img[newidx] = 255;  // Set new spot
    grain[i].pos = newidx;
  }

  display.clearDisplay();
  for (i = 0; i < N_GRAINS; i++) {
    int yPos = grain[i].pos / WIDTH;
    int xPos = grain[i].pos % WIDTH;
    display.drawPixel(xPos , yPos, WHITE);
  }
  display.display();
}


enum SCREENS {
  MAIN_SCREEN = 0,
  POINTS_SCREEN,
  //SAND_SCREEN,
  SCREENS_COUNT
};

int current_screen = MAIN_SCREEN;

void render() {
  switch (current_screen)
  {
  case MAIN_SCREEN:
    displayMain();
    break;
  case POINTS_SCREEN:
    displayPoints();
    break;
  case 500:
    displaySand();
    break;
  default:
    displayMain();
    break;
  }
}

int BOOTLOADER_DELAY = 10000;

void loop() {
  blePeripheral.poll();
  wdt_reset();
  if (sleeping) {
    sd_app_evt_wait();
    sd_nvic_ClearPendingIRQ(SD_EVT_IRQn);
  } else {
    if (millis() - displayRefreshTime > refreshRate) {
      displayRefreshTime = millis();
      render();
      //displayTest();
    }
        
    if (buttonPressed) {
      buttonPressed = false;
      
      current_screen = current_screen+1;
      if(current_screen == SCREENS_COUNT) {
        current_screen = MAIN_SCREEN;
      }

      startbutton = millis();
      while (!digitalRead(BUTTON_PIN)) {}
      if (millis() - startbutton > BOOTLOADER_DELAY ) {
        delay(100);
        int err_code = sd_power_gpregret_set(0x01);
        sd_nvic_SystemReset();
        while (1) {};
      }
    }
      
    if (millis() - sleepTime > sleepDelay ) {
      powerDown();
    }
  }
}
